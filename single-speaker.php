<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<?php $headshot = get_field('headshot'); ?>
		<img src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
		<div>
			<h1><?php the_title(); ?></h1>
			<h2><?php the_field('title'); ?> at <?php the_field('company_name'); ?></h2>
			<?php if( have_rows('social_profiles') ): ?>
				<nav>
				<?php while ( have_rows('social_profiles') ) : the_row(); ?>
					<a href="<?php the_sub_field('profile_link'); ?>"><?php the_sub_field('social_media'); ?></a>
				<?php endwhile; ?>
				</nav>
			<?php endif; ?>
		</div>
	</div>
</header>

<main id="main-content">
	<article>
		<?php the_field('biography'); ?>
	</article>
	<aside>


			<?php
				$args_sessions = array(
					'post_type' => array('session'),
					'posts_per_page' => -1,
					'order' => 'DESC',
					'meta_query' => array(
						'relation'		=> 'AND',
						array(
							'key' => 'speakers',
							'value' => '"' . get_the_ID() . '"', 
							'compare' => 'LIKE'
						),
						array(
							'key'	  	=> 'individual_page',
							'value'	  	=> '1',
							'compare' 	=> '=',
						)
					)
				);
				$sessions = new WP_Query( $args_sessions );
			?>

			<?php if ( $sessions->have_posts() ) : ?>
				<div class="sessions">
					<h3>Speaking Sessions</h3>
					<?php while ( $sessions->have_posts() ) : $sessions->the_post(); ?>
						<?php $terms = get_the_terms( get_the_ID(), 'schedule' ); ?>
						<a href="<?php the_permalink(); ?>">
							<?php foreach ( $terms as $term ) : ?>
								<?php if ( $term->slug = 'day-one' ) : ?>
									<span><?php the_field('day_one','option'); ?></span>
								<?php elseif ( $term->slug = 'day-two' ) : ?>
									<span><?php the_field('day_two','option'); ?></span>
								<?php elseif ( $term->slug = 'day-three' ) : ?>
									<span><?php the_field('day_three','option'); ?></span>
								<?php endif; ?>
							<?php endforeach; ?>
							<?php the_title(); ?>
						</a>
					<?php endwhile; ?>
				</div>
			<?php endif; wp_reset_postdata(); ?>
			</div>
		</div>
		<div class="company">
			<?php $logo = get_field('company_logo'); ?>
			<div class="logo">
				<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
				<?php if ( get_field('subta') == 'yes' ) : ?>
					<div class="badge">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#subta-icon"></use>
						</svg>
						SUBTA Partner
					</div>
				<?php endif; ?>
			</div>
			<h3><?php the_field('company_name'); ?></h3>
			<p><?php the_field('company_description'); ?></p>
			<a href="<?php the_field('company_website'); ?>" target="_blank" class="button is-blue">Visit Website</a>
		</div>
	</aside>
	<section class="cover is-extra-wide" style="background-image: url(https://ffd48znfn3lh.wpengine.com/wp-content/uploads/2019/08/attend-recur-cta-bg-2.jpg);">
		<div>
			<h2>Interested in Speaking?</h2>
			<p>Interested in sharing your experience and expertise with leading businesses in the recurring revenue space? Apply to speak at Recur 2019!</p>
			<a href="<?php the_permalink(422); ?>" class="button">Apply To Speak</a>
		</div>
	</section>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>