var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		STICKY NAV
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= $(window).height()) {
			$(".for-sticky").addClass('stuck');
		} else {
			$(".for-sticky").removeClass('stuck');
		}
	});
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 40) {
			$(".for-mobile").addClass('stuck');
		} else {
			$(".for-mobile").removeClass('stuck');
		}
	});
	/*----------------------------------------------------------------*\
		MOBILE MENU
	\*----------------------------------------------------------------*/
	$("nav.for-mobile button").toggle(
		function () {
			$(this).html("Menu");
			$('nav.for-mobile > .menu').removeClass('is-open');
			$('html').css('overflow', 'auto');
		},
		function () {
			$(this).html("Close");
			$('nav.for-mobile > .menu').toggleClass('is-open');
			$('html').css('overflow', 'hidden');
		}
	).click();
	/*----------------------------------------------------------------*\
		TESTIMONY SLIDER
	\*----------------------------------------------------------------*/
	$('.testimonies').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 5000,
		prevArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M7.92 0l-6 6 6 6 2.1-2.1L6.12 6l3.9-3.9L7.92 0z"/></svg></button>',
		nextArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M1.98 2.1L5.88 6l-3.9 3.9 2.1 2.1 6-6-6-6-2.1 2.1z"/></svg></button>',
		responsive: [{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	/*----------------------------------------------------------------*\
		IMAGE SLIDER
	\*----------------------------------------------------------------*/
	$('.images').slick({
		centerMode: false,
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 5000,
		prevArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M7.92 0l-6 6 6 6 2.1-2.1L6.12 6l3.9-3.9L7.92 0z"/></svg></button>',
		nextArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M1.98 2.1L5.88 6l-3.9 3.9 2.1 2.1 6-6-6-6-2.1 2.1z"/></svg></button>',
	});
	/*----------------------------------------------------------------*\
		SPONSOR SLIDER
	\*----------------------------------------------------------------*/
	$('.sponsors').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		autoplay: true,
		autoplaySpeed: 3000,
		prevArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M7.92 0l-6 6 6 6 2.1-2.1L6.12 6l3.9-3.9L7.92 0z"/></svg></button>',
		nextArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M1.98 2.1L5.88 6l-3.9 3.9 2.1 2.1 6-6-6-6-2.1 2.1z"/></svg></button>',
		responsive: [{
				breakpoint: 1050,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			}, {
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	/*----------------------------------------------------------------*\
		VENUE SLIDER
	\*----------------------------------------------------------------*/
	$('.venue-gallery ul').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		// autoplay: true,
		// autoplaySpeed: 3000,
		prevArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M7.92 0l-6 6 6 6 2.1-2.1L6.12 6l3.9-3.9L7.92 0z"/></svg></button>',
		nextArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M1.98 2.1L5.88 6l-3.9 3.9 2.1 2.1 6-6-6-6-2.1 2.1z"/></svg></button>',
		responsive: [{
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	/*----------------------------------------------------------------*\
		SELECT FIELD
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		LAZYLOAD CLASS FOR IFRAMES
	\*----------------------------------------------------------------*/
	$(function () {
		$('iframe').addClass('lazyload');
	});
});