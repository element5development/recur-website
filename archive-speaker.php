<?php 
/*----------------------------------------------------------------*\

	ARCHIVE TEMPLATE FOR CUSTOM POST TYPE SPEAKER

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1>
	<?php the_field('speaker_title','option'); ?>
		<?php if ( get_field('speaker_intro','option') ) : ?>
			<span><?php the_field('speaker_intro','option'); ?></span>
		<?php endif; ?>
	</h1>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="speaker-feed standard">
				<?php	while ( have_posts() ) : the_post(); ?>
					<a href="<?php the_permalink(); ?>">
						<article class="speaker">
							<?php $headshot = get_field('headshot'); ?>
							<div class="headshot">
								<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
							</div>
							<p><b><?php the_title(); ?></b></p>
							<?php $logo = get_field('company_logo'); ?>
							<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['small']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
						</article>
					</a>
				<?php endwhile; ?>
			</section>
			<section class="cover is-extra-wide" style="background-image: url(https://ffd48znfn3lh.wpengine.com/wp-content/uploads/2019/08/attend-recur-cta-bg-2.jpg);">
				<div>
					<h2>Interested in Speaking?</h2>
					<p>Interested in sharing your experience and expertise with leading businesses in the recurring revenue space? Apply to speak at Recur 2019!</p>
					<a href="<?php the_permalink(422); ?>" class="button">Apply To Speak</a>
				</div>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>