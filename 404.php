<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/elements/navigation'); ?>

<header id="main-content" class="post-head">
	<div>
		<h1>Seems this page is missing information. <span>Let's start back at our <a href="<?php echo get_home_url(); ?>">home page</a>.</span></h1>
		<a href="<?php echo get_home_url(); ?>" class="button	">Back to Home</a>
	</div>
</header>


<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>