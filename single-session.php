<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php 
	if ( has_term('day-one', 'schedule') ) :
		$day = get_field('day_one', 'option');
	elseif ( has_term('day-two', 'schedule') ) :
		$day = get_field('day_two', 'option');
	elseif ( has_term('day-three', 'schedule') ) :
		$day = get_field('day_three', 'option');
	endif;
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1>
		<?php the_title(); ?>
		<span><?php echo $day; ?> at <?php the_field('start'); ?></span>
		<?php if ( get_field('topic') == 'media' ) : ?>
			<svg viewBox="0 0 50 50">
				<use xlink:href="#pin-media"></use>
			</svg>
		<?php elseif ( get_field('topic') == 'saas' ) : ?>
			<svg viewBox="0 0 50 50">
				<use xlink:href="#pin-saas"></use>
			</svg>
		<?php elseif ( get_field('topic') == 'subbox' ) : ?>
			<svg viewBox="0 0 50 50">
				<use xlink:href="#pin-subbox"></use>
			</svg>
		<?php elseif ( get_field('topic') == 'digital' ) : ?>
			<svg viewBox="0 0 50 50">
				<use xlink:href="#pin-digital"></use>
			</svg>
		<?php elseif ( get_field('topic') == 'memberships' ) : ?>
			<svg viewBox="0 0 50 50">
				<use xlink:href="#pin-memberships"></use>
			</svg>
		<?php elseif ( get_field('topic') == 'subsave' ) : ?>
			<svg viewBox="0 0 50 50">
				<use xlink:href="#pin-subsave"></use>
			</svg>
		<?php else : ?>
			<!-- nothing -->
		<?php endif; ?>
	</h1>
</header>

<main id="main-content">
	<article>
		<?php the_field('full_description'); ?>
	</article>
	<aside>
		<?php $posts = get_field('speakers'); ?>
		<?php foreach( $posts as $post): setup_postdata($post); ?>
			<div class="speaker">
				<?php $headshot = get_field('headshot'); ?>
				<img src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
				<div>
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('title'); ?> at <?php the_field('company_name'); ?></p>
					<a href="<?php the_permalink(); ?>">Learn More</a>
				</div>
			</div>
		<?php endforeach; wp_reset_postdata(); ?>
	</aside>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>