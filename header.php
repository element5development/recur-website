<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>

	<?php //general stuff ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name="msvalidate.01" content="D7FF62B50B5AC4465FB383958CD4A132" />
	<?php
	/*----------------------------------------------------------------*\
	|
	|	title, social and other seo tags are all handled and inserted 
	| via The SEO Framework which is a must have plugin
	|
	\*----------------------------------------------------------------*/
	?>
	<?php // force Internet Explorer to use the latest rendering engine available ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MDT8FSP');</script>
	<!-- End Google Tag Manager -->
	<!-- Hotjar Tracking Code for https://recurcon.com/ -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:1462002,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MDT8FSP"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<a id="skip-to-content" href="#main-content">Skip to main content</a>

	<?php get_template_part('template-parts/icon-set'); ?>