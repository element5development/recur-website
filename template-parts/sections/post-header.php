<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<?php //TITLE LENGTH
	if ( strlen(get_the_title()) >= 67 ) :
		$length = 'long';
	else :
		$length = 'normal';
	endif;
?>

<header class="post-head">
	<div>
		<h1 class="<?php echo $length; ?>">
			<?php the_title(); ?>
			<?php if ( get_field('subheader') ) : ?>
				<span><?php the_field('subheader'); ?></span>
			<?php endif; ?>
		</h1>
		<?php if( have_rows('header_buttons') ): $i = 1; ?>
			<?php while ( have_rows('header_buttons') ) : the_row(); ?>
				<?php 
					$link = get_sub_field('button');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					if ($i == 1 ) :
						$color = 'is-orange';
					elseif ($i == 2 ) : 
						$color = 'is-blue';
					else :
						$color = 'is-white';
					endif;
				?>
				<a class="button <?php echo $color; ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
    	<?php $i++; endwhile; ?>
		<?php endif; ?>
	</div>
</header>