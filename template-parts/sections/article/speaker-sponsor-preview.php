<?php 
/*----------------------------------------------------------------*\

	GRID OF FEATURED SPEAKERS FOLLOWED BY A SLIDER OF SPONSORS
	4 column grid

\*----------------------------------------------------------------*/
?>

<?php //featured speaker loop
	$args_speakers = array(
		'post_type' => array('speaker'),
		'posts_per_page' => 8,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'featured',
            'value' => 'yes',
        )
    )
	);
	$speakers = new WP_Query( $args_speakers );
?>

<?php //sponsor loop
	$args_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'featured',
            'value' => 'yes',
        )
    )
	);
	$sponsors = new WP_Query( $args_sponsors );
?>

<section class="speakers-and-sponsors is-full-width">
	<h2>Be Surrounded By<span>Industry Leaders and Premium Vendors</span></h2>
	<a href="<?php echo get_site_url(); ?>/speaker/" class="button is-white">View All Speakers</a>
	<a href="<?php echo get_site_url(); ?>/sponsor/" class="button is-white">View All Sponsors</a>
	<?php if ( $speakers->have_posts() ) : ?>
		<div class="speakers is-extra-wide">
			<?php while ( $speakers->have_posts() ) : $speakers->the_post(); ?>
				<div class="speaker">
					<?php $headshot = get_field('headshot'); ?>
					<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
					<p><b><?php the_title(); ?></b></p>
					<p><?php the_field('title'); ?></p>
					<p><?php the_field('company_name'); ?></p>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; wp_reset_postdata(); ?>
	<?php if ( $sponsors->have_posts() ) : ?>
		<div class="sponsors is-extra-wide">
			<?php while ( $sponsors->have_posts() ) : $sponsors->the_post(); ?>
				<div class="sponsor">
					<div class="container">
						<?php $logo = get_field('logo'); ?>
						<img src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; wp_reset_postdata(); ?>
</section>