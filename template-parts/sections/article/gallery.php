<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>

<?php //NUMBER TO TEXT
	$number = new NumberFormatter("en", NumberFormatter::SPELLOUT);
	$column_count = $number->format(get_sub_field('columns'));
?>

<?php //GALLERY
	$images = get_sub_field('gallery');
?>

<section class="gallery <?php the_sub_field('width'); ?> <?php echo $column_count; ?>-columns">
	<?php foreach( $images as $image ): ?>
		<figure>
			<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 600w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $image['alt']; ?>">
		</figure>
	<?php endforeach; ?>
</section>
