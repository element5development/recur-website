<?php 
/*----------------------------------------------------------------*\

	COUNTDOWN
	displaying countdown to day one and get ticket button

\*----------------------------------------------------------------*/
?>

<section class="countdown is-full-width">
	<div class="is-extra-wide">
		<h2>
			Register today for Recur 2019
			<span>
				<?php the_field('day_one', 'option'); ?> - <?php the_field('day_three', 'option'); ?>
			</span>
		</h2>
		<div class="counter">
			<div class="weeks">
				<p>
					<span>##</span>
					weeks
				</p>
			</div>
			<div class="days">
				<p>
					<span>##</span>
					days
				</p>
			</div>
			<div class="hours">
				<p>
					<span>##</span>
					hours
				</p>
			</div>
			<div class="minutes">
				<p>
					<span>##</span>
					minutes
				</p>
			</div>
			<div class="seconds">
				<p>
					<span>##</span>
					seconds
				</p>
			</div>
		</div>
		<a href="<?php the_permalink(421); ?>" class="button is-blue">Get Your Tickets</a>
	</div>
</section>