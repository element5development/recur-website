<?php 
/*----------------------------------------------------------------*\

	TESTIMONY SLIDER
	displaying slider of testimonies showing 3 at a time
	
\*----------------------------------------------------------------*/
?>

<?php
$args_testimonies = array(
	'post_type' => array('testimony'),
	'posts_per_page' => -1,
	'nopaging' => true,
	'order' => 'DESC',
);
$testimonies = new WP_Query( $args_testimonies );
?>

<?php if ( $testimonies->have_posts() ) : ?>
	<section class="testimony-slider is-extra-wide">
		<h2>Discover What Attendees Are Saying</h2>
		<div class="testimonies">
			<?php while ( $testimonies->have_posts() ) : $testimonies->the_post(); ?>
				<div class="testimony">
					<p><?php the_field('quote'); ?></p>
					<h3><?php the_title(); ?><span><?php the_field('decription'); ?></span></h3>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; wp_reset_postdata(); ?>