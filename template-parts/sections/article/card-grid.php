<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>

<?php //NUMBER TO TEXT
	if ( get_sub_field('columns_wide') ) :
		$number = new NumberFormatter("en", NumberFormatter::SPELLOUT);
		$column_count = $number->format(get_sub_field('columns_wide'));
	else :
		$number = new NumberFormatter("en", NumberFormatter::SPELLOUT);
		$column_count = $number->format(get_sub_field('columns'));
	endif;
?>

<section class="card-grid <?php the_sub_field('format'); ?>-cards <?php the_sub_field('width'); ?> <?php echo $column_count; ?>-columns">
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<div class="card <?php if ( !get_sub_field('button') ) : ?>has-no-button<?php endif; ?>">
			<!-- IMAGE -->
			<?php $image = get_sub_field('image'); ?>
			<?php if ( get_sub_field('image') ) : ?>
				<div class="bg" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);"></div>
			<?php endif; ?>
			<!-- HEADLINE -->
			<?php if ( get_sub_field('title') ) : ?>
				<h3><?php the_sub_field('title') ?></h3>
			<?php endif; ?>
			<!-- DESCRIPTION -->	
			<?php if ( get_sub_field('description') ) : ?>
				<p><?php the_sub_field('description'); ?></p>
			<?php endif; ?>
			<!-- BUTTON -->
			<?php
				$link = get_sub_field('button'); 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self'; 
				if ( get_sub_field('button') ) : 
			?>
				<a class="button is-blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
					<?php echo esc_html($link_title); ?>
				</a>
			<?php endif; ?>
		</div>
	<?php endwhile; ?>
</section>