<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

	Majority content was commented out by lyle when Recur was merged
	into SubSummit on 1/7/20

\*----------------------------------------------------------------*/
?>

<!-- <section class="get-tickets-banner">
	<h2>Register for Recur 2019 <span>Sunday, October 20th – Tuesday, October 22, 2019</span></h2>
	<a href="<?php the_permalink(421); ?>" class="button is-blue">RSVP for Recur 2020</a>
</section> -->

<footer class="post-footer">
	<div class="brand">
		<a href="<?php echo get_home_url(); ?>">
			<svg viewBox="0 0 489 127">
				<use xlink:href="#recur"></use>
			</svg>
		</a>
		<!-- <p>Recur is powered by SUBTA, the Subsciption Trade Association</p>
		<a href="https://subta.com/" target="_blank" class="button is-red">Join Subta</a> -->
	</div>

	<!-- <div class="menus">
		<div class="menu">
			<?php dynamic_sidebar( 'Footer Column One' ); ?>
		</div>
		<div class="menu">
			<?php dynamic_sidebar( 'Footer Column Two' ); ?>
		</div>
		<div class="menu">
			<?php dynamic_sidebar( 'Footer Column Three' ); ?>
		</div>
	</div> -->

	<div class="profiles">
		<a href="https://twitter.com/_SUBTA_" target="_blank" class="profile twitter">
			<svg viewBox="0 0 32 32">
				<use xlink:href="#twitter"></use>
			</svg>
		</a>
		<a href="https://www.instagram.com/_subta_/" target="_blank" class="profile instagram">
			<svg viewBox="0 0 32 32">
				<use xlink:href="#instagram"></use>
			</svg>
		</a>
		<a href="https://www.facebook.com/SUBSCRIPTIONTRADEASSOCIATION/" target="_blank" class="profile facebook">
			<svg viewBox="0 0 32 32">
				<use xlink:href="#facebook"></use>
			</svg>
		</a>
		<a href="https://www.linkedin.com/company/subta/" target="_blank" class="profile linkedin">
			<svg viewBox="0 0 32 32">
				<use xlink:href="#linkedin"></use>
			</svg>
		</a>
		<a href="https://www.youtube.com/channel/UCSSrvzhyrt01g3Adx4z98BQ/playlists" target="_blank" class="profile youtube">
			<svg viewBox="0 0 32 32">
				<use xlink:href="#youtube"></use>
			</svg>
		</a>
	</div>
	
	<div class="copyright">
		<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.<a href="<?php the_permalink('323'); ?>">Privacy Policy</a> <a href="<?php the_permalink('432'); ?>">Refund Policy</a></p>
		<a target="_blank" href="https://element5digital.com"> 
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/element5.svg" alt="Crafted by Element5 Digital"> 
		</a>
	</div>
</footer>