<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

	Majority content was commented out by lyle when Recur was merged
	into SubSummit on 1/7/20

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<div class="subta">
		<p>Recur is powered by
			<svg viewBox="0 0 489 127">
				<use xlink:href="#subta"></use>
			</svg>
		</p>
		<p><span>SUBTA is the Subscription Trade Association.</span>
			<a target="_blank" href="https://subta.com/">Join SUBTA today!
				<svg viewBox="0 0 50 50">
					<use xlink:href="#arrow-right"></use>
				</svg>
			</a>
		</p>
	</div>
	<!-- <nav class="for-sticky">
		<a href="<?php echo get_home_url(); ?>">
			<svg viewBox="0 0 489 127">
				<use xlink:href="#recur"></use>
			</svg>
		</a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
	</nav> -->
	<!-- <nav class="for-desktop">
		<a href="<?php echo get_home_url(); ?>">
			<svg viewBox="0 0 489 127">
				<use xlink:href="#recur"></use>
			</svg>
			<span class="location">Chicago</span>
		</a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
	</nav> -->
	<!-- <nav class="for-mobile">
		<a href="<?php echo get_home_url(); ?>">
			<svg viewBox="0 0 489 127">
				<use xlink:href="#recur"></use>
			</svg>
			<span class="location">Chicago</span>
		</a>
		<button class="is-burgundy">Menu</button>
		<div class="menu">
			<?php wp_nav_menu(array( 'theme_location' => 'mobile_navigation' )); ?>
		</div>
	</nav> -->
</div>