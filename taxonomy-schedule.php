<?php 
/*----------------------------------------------------------------*\

	ARCHIVE TEMPLATE FOR CUSTOM POST TYPE SESSION

\*----------------------------------------------------------------*/
?>

<!-- query posts and make an array of all start times with no duplicates -->
<?php	
	$sessions = array();
	$day = get_queried_object()->slug;
	while ( have_posts() ) : the_post(); 
		$start = get_field('start');
		$start = date("H:i", strtotime($start));
		if (!in_array($start, $sessions)) :
			$sessions[] = $start; 
		endif;
	endwhile; 
	sort($sessions);
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1>
	<?php the_field('session_title','option'); ?>
		<?php if ( get_field('session_intro','option') ) : ?>
			<span><?php the_field('session_intro','option'); ?></span>
		<?php endif; ?>
	</h1>
</header>

<main id="main-content">
	<nav>
		<div>
			<a href="<?php echo get_home_url(); ?>/schedule/day-one/" <?php if ( get_queried_object()->slug == 'day-one' ) : ?>class="active"<?php endif; ?>><?php the_field('day_one', 'option'); ?></a>
			<a href="<?php echo get_home_url(); ?>/schedule/day-two/" <?php if ( get_queried_object()->slug == 'day-two' ) : ?>class="active"<?php endif; ?>><?php the_field('day_two', 'option'); ?></a>
			<a href="<?php echo get_home_url(); ?>/schedule/day-three/" <?php if ( get_queried_object()->slug == 'day-three' ) : ?>class="active"<?php endif; ?>><?php the_field('day_three', 'option'); ?></a>
		</div>
	</nav>
	<article>
		<?php if ( get_queried_object()->slug == 'day-one' ) : ?>
			<h2><?php the_field('day_one', 'option'); ?></h2>
		<?php elseif ( get_queried_object()->slug == 'day-two' ) : ?>
			<h2><?php the_field('day_two', 'option'); ?></h2>
		<?php elseif ( get_queried_object()->slug == 'day-three' ) : ?>
			<h2><?php the_field('day_three', 'option'); ?></h2>
		<?php else : ?>
			<!-- do nothing -->
		<?php endif; ?>
		<?php foreach ($sessions as $session) : ?>
			<?php
				$session12 = date("g:i a", strtotime($session));
				$start_time = new DateTime($session12);
				$start_time = $start_time->format('H:i:s');
				$args_session_block = array(
					'post_type' => array('session'),
					'posts_per_page' => -1,
					'order' => 'DESC',
					'tax_query' => array(
						array(
							'taxonomy' => 'schedule',
							'field' => 'slug',
							'terms' => array($day),
							'operator' => 'IN',
						),
					),
					'meta_query'	=> array(
						array(
							'key'	  	=> 'start',
							'value'	  	=> $start_time,
							'compare' 	=> '=',
						),
					),
				);
				$session_block = new WP_Query( $args_session_block );
			?>
			<?php if ( $session_block->have_posts() ) : ?>
				<section class="sessions standard">
					<?php while ( $session_block->have_posts() ) : $session_block->the_post(); ?>
						<article class="<?php the_field('format'); ?> session basic">
							<?php if ( get_field('format') == 'speaker' || get_field('individual_page') == 'yes' ) : ?>
								<a href="<?php the_permalink(); ?>"></a>
							<?php endif; ?>
							<div class="tags">
								<p><?php the_field('start'); ?></p>
								<?php if ( get_field('topic') == 'media' ) : ?>
									<svg viewBox="0 0 50 50">
										<use xlink:href="#pin-media"></use>
									</svg>
								<?php elseif ( get_field('topic') == 'saas' ) : ?>
									<svg viewBox="0 0 50 50">
										<use xlink:href="#pin-saas"></use>
									</svg>
								<?php elseif ( get_field('topic') == 'subbox' ) : ?>
									<svg viewBox="0 0 50 50">
										<use xlink:href="#pin-subbox"></use>
									</svg>
								<?php elseif ( get_field('topic') == 'digital' ) : ?>
									<svg viewBox="0 0 50 50">
										<use xlink:href="#pin-digital"></use>
									</svg>
								<?php elseif ( get_field('topic') == 'memberships' ) : ?>
									<svg viewBox="0 0 50 50">
										<use xlink:href="#pin-memberships"></use>
									</svg>
								<?php elseif ( get_field('topic') == 'subsave' ) : ?>
									<svg viewBox="0 0 50 50">
										<use xlink:href="#pin-subsave"></use>
									</svg>
								<?php else : ?>
									<!-- nothing -->
								<?php endif; ?>
							</div>
							<div class="contents">
								<div class="title">
									<h3><?php the_title(); ?></h3>
									<?php if ( get_field('short_description') ) : ?>
										<?php the_field('short_description'); ?>
									<?php endif; ?>
								</div>
								<?php if ( get_field('speakers') ) : ?>
									<?php $posts = get_field('speakers'); ?>
									<div class="speakers">
										<p>Presented By:</p>
										<?php foreach( $posts as $post): setup_postdata($post); ?>
											<div class="speaker">
												<?php $headshot = get_field('headshot'); ?>
												<img class="lazyload blur-up" data-expand="-0" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['thumbnail']; ?>" alt="<?php echo $headshot['alt']; ?>" />
												<p><span><?php the_title(); ?></span> <?php the_field('title'); ?> at <?php the_field('company_name'); ?></p>
												<?php $logo = get_field('company_logo'); ?>
												<img class="lazyload blur-up" data-expand="-0" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
											</div>
										<?php endforeach; wp_reset_postdata(); ?>
									</div>
								<?php endif; ?>
							</div>
						</article>
					<?php endwhile; ?>
				</section>
			<?php endif; wp_reset_postdata(); ?>
		<?php endforeach; ?>
	</article>
	<?php if ( get_queried_object()->slug == 'day-one' ) : ?>
		<a href="<?php echo get_home_url(); ?>/schedule/day-two/" class="button is-white">View The Next Day</a>
	<?php elseif ( get_queried_object()->slug == 'day-two' ) : ?>
		<a href="<?php echo get_home_url(); ?>/schedule/day-three/" class="button is-white">View The Next Day</a>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>