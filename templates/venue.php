<?php 
/*----------------------------------------------------------------*\

	Template Name: Venue
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<?php $image = get_field('background_image'); ?>
		<section class="location is-extra-wide" style="background-image: url(<?php echo $image['sizes']['xlarge']; ?>);">
			<div>
				<h2><?php the_field('location_name'); ?><span><?php the_field('location_address'); ?></span></h2>
				<p><?php the_field('location_description'); ?></p>
				<a href="<?php the_field('booking'); ?>" target="_blank" class="button is-orange">Book Your Room</a>
				<a href="<?php the_field('map'); ?>" target="_blank" class="button is-blue">Get Directions</a>
			</div>
		</section>
		<section class="venue-gallery is-extra-wide">
			<?php $images = get_field('gallery_images'); ?>
			<?php if( $images ): ?>
				<ul>
					<?php foreach( $images as $image ): ?>
						<li>
							<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['small']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 600w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $image['alt']; ?>">
						</li>
					<?php endforeach; ?>
    		</ul>
			<?php endif; ?>
		</section>
		<?php if( have_rows('additional_activities') ): ?>
			<section class="additional-activities is-extra-wide">
				<h3>Additional Activities</h3>
				<p>Restaurants, Tours, Sightseeing, Yoga, and More</p>
				<a href="<?php the_permalink(891) ?>" class="button is-white">Host your own event</a>
				<section class="card-grid cover-cards is-extra-wide four-columns">
					<?php while ( have_rows('additional_activities') ) : the_row(); ?>
						<div class="card">
							<?php $image = get_sub_field('image'); ?>
							<div class="bg" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);"></div>
							<div class="button is-blue"><?php the_sub_field('title'); ?></div>
						</div>
					<?php endwhile; ?>
				</section>
			</section>
		<?php endif; ?>
		<?php get_template_part('template-parts/sections/article/testimony-slider'); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>