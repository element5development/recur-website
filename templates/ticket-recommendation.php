<?php 
/*----------------------------------------------------------------*\

	Template Name: Ticket Recommendation
	
\*----------------------------------------------------------------*/
?>

<?php //tickets to show
	$type = $_GET["type"];

	if ( $type=='supplier' ) :
		$member = 'SUBTA partner'; 
	elseif ( $type == 'merchant' ) :
		$member = 'SUBTA member'; 
	else :
		$member = 'general';
	endif;
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<?php if ( is_null($type) ) : ?>
			<section class="question phaseOne standard">
				<h2>Are you a merchant or a supplier?</h2>
				<a href="<?php the_permalink(); ?>/?type=merchant" class="card">
					<h3>I’m a Merchant</h3>
					<p>An individual from a subscription commerce business (Subscription Box, Software as a Service (SaaS), Membership, Media Subscriptions, Subscribe and Save, Digital Subscription)</p>
				</a>
				<a href="<?php the_permalink(); ?>/?type=supplier" class="card">
					<h3>I’m a Supplier</h3>
					<p>An individual from a supplier that supports merchants in the subscription commerce economy</p>
				</a>
				<!-- <a href="/tickets/" class="button is-white">Choose For Yourself</a> -->
			</section>
		<?php else : ?>
			<section class="question phaseTwo standard">
				<h2>Are you a SUBTA member?</h2>
				<?php if ( $type=='supplier' ) : ?>
					<a href="/tickets/?type=<?php echo $type; ?>&member=<?php echo $member; ?> " class="card">
						<h3>Yes</h3>
						<p>For individuals of vendors/suppliers in the subscription commerce space that have joined SUBTA</p>
					</a>
					<a href="/tickets/?type=<?php echo $type; ?>&member=general" class="card">
						<h3>No</h3>
						<p> For individuals of vendors/suppliers in the subscription commerce space that have <b>not</b> joined SUBTA</p>
					</a>
					<a href="https://subta.com/join/become-a-partner/" target="_blank" class="button is-white">Discover SUBTA Member Perks</a>
				<?php else : ?>
					<a href="/tickets/?type=<?php echo $type; ?>&member=<?php echo $member; ?> " class="card">
						<h3>Yes</h3>
						<p>For individuals of subscription-based businesses that have joined SUBTA</p>
					</a>
					<a href="/tickets/?type=<?php echo $type; ?>&member=general" class="card">
						<h3>No</h3>
						<p>For individuals of subscription-based business that have <b>not</b> joined SUBTA</p>
					</a>
					<a href="https://subta.com/join/become-a-member/" target="_blank" class="button is-white">Discover SUBTA Member Perks</a>
				<?php endif; ?>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>