<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header id="main-content" class="post-head">
	<div>
		<h1><?php the_title(); ?> <span><?php the_field('message'); ?></span></h1>
	</div>
</header>
<?php if ( is_page(923) ) : //FREE TICKET CONFIRMATION ?>
	<section class="standard" style="text-align: center; margin: var(--xLarge) auto;">
		<h3>Know anyone else who might want to attend Recur?</h3>
		<p style="margin-bottom: var(--Large);">Share the opportunity to get free tickets with them now.</p>
		<a class="button" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Frecurcon.com%2Ffree-ticket-application">Facebook</a>
		<a class="button" href="https://twitter.com/intent/tweet?url=https%3A%2F%2Frecurcon.com%2Ffree-ticket-application&text=Come%20join%20me%20at%20Recur%20October%2020%E2%80%9322.%20All%20subscription-based%20businesses%20may%20apply%20to%20receive%20free%20or%20heavily%20discounted%20tickets!">Twitter</a>
		<a class="button" href="http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Frecurcon.com%2Ffree-ticket-application&title=Come%20join%20me%20at%20Recur%20October%2020%E2%80%9322.%20All%20subscription-based%20businesses%20may%20apply%20to%20receive%20free%20or%20heavily%20discounted%20tickets!">Linkedin</a>
		<a class="button" href="mailto:example@gmail.com?subject=Come%20join%20me%20at%20Recur%20October%2020%E2%80%9322&amp;body=All%20subscription-based%20businesses%20may%20apply%20to%20receive%20free%20or%20heavily%20discounted%20tickets!%20https%3A%2F%2Frecurcon.com%2Ffree-ticket-application">Email</a>
	</section>
<?php endif; ?>
<?php if ( is_page(424) ) : //Sponsor confirmation ?>
	<section class="standard">
		<div class="calendly-inline-widget" data-url="https://calendly.com/jpbrophy/subta" style="min-width:320px;height:1024px;"></div>
		<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
	</section>
<?php endif; ?>

<!-- <?php get_template_part('template-parts/sections/article/countdown'); ?> -->

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>