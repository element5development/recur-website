<?php 
/*----------------------------------------------------------------*\

	Template Name: Sidebar
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<div class="article-aside">
		<article>
			<?php
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'editor' ):
						get_template_part('template-parts/sections/article/editor');
					elseif( get_row_layout() == '2editor' ):
						get_template_part('template-parts/sections/article/editor-2-column');
					elseif( get_row_layout() == '3editor' ):
						get_template_part('template-parts/sections/article/editor-3-column');
					elseif( get_row_layout() == 'media+text' ):
						get_template_part('template-parts/sections/article/media-text');
					elseif( get_row_layout() == 'cover' ):
						get_template_part('template-parts/sections/article/cover');
					elseif( get_row_layout() == 'gallery' ):
						get_template_part('template-parts/sections/article/gallery');
					elseif( get_row_layout() == 'card_grid' ):
						get_template_part('template-parts/sections/article/card-grid');
					elseif( get_row_layout() == 'speakers_sponsors' ):
						get_template_part('template-parts/sections/article/speaker-sponsor-preview');
					elseif( get_row_layout() == 'testimony_slider' ):
						get_template_part('template-parts/sections/article/testimony-slider');
					elseif( get_row_layout() == 'countdown' ):
						get_template_part('template-parts/sections/article/countdown');
					endif;
				endwhile;
			?>
		</article>
		<?php if( have_rows('sidebar_widgets') ): ?>
			<aside>
				<?php while ( have_rows('sidebar_widgets') ) : the_row(); ?>
					<div class="text-widget">
						<h3><?php the_sub_field('title'); ?></h3>
						<?php the_sub_field('description'); ?>
						<?php 
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" class="button is-blue" ><?php echo esc_html($link_title); ?></a>
					</div>
				<?php endwhile; ?>
			</aside>
		<?php endif; ?>
	</div>
	<?php get_template_part('template-parts/sections/article/speaker-sponsor-preview'); ?>
	<?php get_template_part('template-parts/sections/article/testimony-slider'); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>