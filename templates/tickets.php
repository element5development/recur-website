<?php 
/*----------------------------------------------------------------*\

	Template Name: Ticket Purchasing
	
\*----------------------------------------------------------------*/
?>

<?php //tickets to show
	$type = $_GET["type"];
	$member = $_GET["member"];
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<?php while ( have_rows('tickets') ) : the_row(); ?>
			<?php if ( get_sub_field('merchant_or_vendor') == $type || is_null($type) ) : ?>
				<?php if ( get_sub_field('member') == $member || is_null($member) ) : ?>
					<div class="ticket is-extra-wide <?php the_sub_field('color') ?>">
						<div>
							<h2><span><?php the_sub_field('merchant_or_vendor'); ?> Ticket</span><?php the_sub_field('member'); ?> Admission</h2>
							<p><?php the_sub_field('description'); ?></p>
						</div>
						<div>
							<p>$<?php the_sub_field('price'); ?></p>
							<?php 
								$link = get_sub_field('button');
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a class="button is-white" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		<?php endwhile; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>