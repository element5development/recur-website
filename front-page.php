<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

	Majority content was commented out by lyle when Recur was merged
	into SubSummit on 1/7/20

\*----------------------------------------------------------------*/
?>

<?php 
	//speaker loop
	// $args_speakers = array(
	// 	'post_type' => array('speaker'),
	// 	'posts_per_page' => -1,
	// 	'nopaging' => true,
	// 	'order' => 'DESC',
	// );
	// $speakers = new WP_Query( $args_speakers );

	//sponsor loop
	// $args_sponsors = array(
	// 	'post_type' => array('sponsor'),
	// 	'posts_per_page' => -1,
	// 	'nopaging' => true,
	// 	'order' => 'DESC',
	// 	'meta_query' => array(
  //       array(
  //           'key' => 'featured',
  //           'value' => 'yes',
  //       )
  //   )
	// );
	// $sponsors = new WP_Query( $args_sponsors );
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<svg viewBox="0 0 489 127">
			<use xlink:href="#recur"></use>
		</svg>
		<h1><?php the_title(); ?></h1>
		<p><?php the_field('subheader'); ?></p>
		<?php 
			$link = get_field('header_button');
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<a class="button is-blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
	</div>
</header>

<main id="main-content">
	<article>
		<section class="intro standard">
			<?php the_field('intro'); ?>
		</section>

		<!-- <section class="cover is-extra-wide" style="background-image: url(<?php echo get_site_url(); ?>/wp-content/uploads/2019/08/attend-recur-cta-bg-2.jpg);">
			<div>
				<?php the_field('banner'); ?>
			</div>
		</section> -->

		<!-- <section class="editor-2-column is-extra-wide">
			<h2><?php the_field('columns_title'); ?></h2>
			<div>
				<?php the_field('column_left'); ?>
			</div>
			<div>
				<?php the_field('column_right'); ?>
			</div>
		</section> -->

		<!-- <section class="image-slider is-full-width">
			<div class="images">
				<?php if( have_rows('image_slider') ): ?>
					<?php while ( have_rows('image_slider') ) : the_row(); ?>
						<?php $sliderimage = get_sub_field('image'); ?>
						<?php if ( get_sub_field('image') ) : ?>
							<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $sliderimage['sizes']['placeholder']; ?>" data-src="<?php echo $sliderimage['sizes']['large']; ?>" data-srcset="<?php echo $sliderimage['sizes']['small']; ?> 350w, <?php echo $sliderimage['sizes']['medium']; ?> 600w, <?php echo $sliderimage['sizes']['large']; ?> 1000w, <?php echo $sliderimage['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $sliderimage['alt']; ?>">
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section> -->

		<section class="card-grid top-cards is-extra-wide two-columns">
			<h2>Stay connected and continue to grow</h2>
			<?php while ( have_rows('cards') ) : the_row(); ?>
				<div class="card">
					<?php $image = get_sub_field('logo'); ?>
					<?php if ( get_sub_field('logo') ) : ?>
						<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 600w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $image['alt']; ?>">
					<?php endif; ?>
					<h3><?php the_sub_field('title') ?></h3>
					<p><?php the_sub_field('description'); ?></p>
					<?php
						$link = get_sub_field('button'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						$button = get_sub_field('button_two'); 
						$button_url = $button['url'];
						$button_title = $button['title'];
						$button_target = $button['target'] ? $button['target'] : '_self';  
					?>
					<div class="buttons">
						<?php if ( get_sub_field('button') ) : ?>
							<a class="button is-blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
								<?php echo esc_html($link_title); ?>
							</a>
						<?php endif; ?>
						<?php if ( get_sub_field('button_two') ) : ?>
							<a class="button is-blue" href="<?php echo esc_url($button_url); ?>" target="<?php echo esc_attr($button_target); ?>">
								<?php echo esc_html($button_title); ?>
							</a>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
		</section>
		<div class="sand-block"></div>

		<!-- <?php get_template_part('template-parts/sections/article/testimony-slider'); ?> -->

		<!-- <?php get_template_part('template-parts/sections/article/countdown'); ?> -->
		
		<!-- <section class="speakers-and-sponsors is-full-width">
			<h2>Thank You to our Speakers and Sponsors</h2>
			<a href="<?php echo get_site_url(); ?>/speaker" class="button is-white">View All Speakers</a>
			<a href="<?php echo get_site_url(); ?>/sponsor/" class="button is-white">View All Sponsors</a>
			<?php // if ( $speakers->have_posts() ) : ?>
				<div class="speakers is-extra-wide">
					<?php // while ( $speakers->have_posts() ) : $speakers->the_post(); ?>
						<div class="speaker">
							<?php $headshot = get_field('headshot'); ?>
							<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
							<p><b><?php the_title(); ?></b></p>
							<?php $logo = get_field('company_logo'); ?>
							<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['small']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
						</div>
					<?php // endwhile; ?>
				</div>
			<?php // endif; wp_reset_postdata(); ?>
			<?php // if ( $sponsors->have_posts() ) : ?>
				<div class="sponsors is-extra-wide">
					<?php // while ( $sponsors->have_posts() ) : $sponsors->the_post(); ?>
						<div class="sponsor">
							<div class="container">
								<?php $logo = get_field('logo'); ?>
								<img src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
							</div>
						</div>
					<?php // endwhile; ?>
				</div>
			<?php // endif; wp_reset_postdata(); ?>
		</section> -->

	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>