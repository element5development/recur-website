<?php
/*----------------------------------------------------------------*\
	INITIALIZE TAXONOMIES
\*----------------------------------------------------------------*/
// Register Taxonomy Schedule
function create_schedule_tax() {
	$labels = array(
		'name'              => _x( 'Schedule', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Schedule', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Schedule', 'textdomain' ),
		'all_items'         => __( 'All Schedule', 'textdomain' ),
		'parent_item'       => __( 'Parent Schedule', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Schedule:', 'textdomain' ),
		'edit_item'         => __( 'Edit Schedule', 'textdomain' ),
		'update_item'       => __( 'Update Schedule', 'textdomain' ),
		'add_new_item'      => __( 'Add New Schedule', 'textdomain' ),
		'new_item_name'     => __( 'New Schedule Name', 'textdomain' ),
		'menu_name'         => __( 'Schedule', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => false,
		'meta_box_cb' => false,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'schedule', array('session'), $args );
}
add_action( 'init', 'create_schedule_tax' );