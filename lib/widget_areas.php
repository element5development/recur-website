<?php
/*----------------------------------------------------------------*\
	INITIALIZE WIDGET AREA
\*----------------------------------------------------------------*/
function custom_sidebars() {
	$args = array(
		'name'          => __( 'Footer Column One' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<p>',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Column Two' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<p>',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Column Three' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<p>',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'custom_sidebars' );