<?php 
/*----------------------------------------------------------------*\

	ARCHIVE TEMPLATE FOR CUSTOM POST TYPE SPONSOR

\*----------------------------------------------------------------*/
?>

<?php //platinum sponsors loop
	$args_platinum_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'platinum',
        )
    )
	);
	$platinum_sponsors = new WP_Query( $args_platinum_sponsors );
?>
<?php //gold sponsors loop
	$args_gold_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'gold',
        )
    )
	);
	$gold_sponsors = new WP_Query( $args_gold_sponsors );
?>
<?php //silver sponsors loop
	$args_silver_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'silver',
        )
    )
	);
	$silver_sponsors = new WP_Query( $args_silver_sponsors );
?>
<?php //bronze sponsors loop
	$args_bronze_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'bronze',
        )
    )
	);
	$bronze_sponsors = new WP_Query( $args_bronze_sponsors );
?>
<?php //other sponsors loop
	$args_other_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'other',
        )
    )
	);
	$other_sponsors = new WP_Query( $args_other_sponsors );
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1>
	<?php the_field('sponsor_title','option'); ?>
		<?php if ( get_field('sponsor_intro','option') ) : ?>
			<span><?php the_field('sponsor_intro','option'); ?></span>
		<?php endif; ?>
	</h1>
</header>

<main id="main-content">
	<article>
		<section class="card-grid text-cards is-extra-wide two-columns">
			<?php while ( have_rows('sponsor_cards', 'option') ) : the_row(); ?>
				<div class="card <?php if ( !get_sub_field('button') ) : ?>has-no-button<?php endif; ?>">
					<!-- HEADLINE -->
					<?php if ( get_sub_field('title') ) : ?>
						<h3><?php the_sub_field('title') ?></h3>
					<?php endif; ?>
					<!-- DESCRIPTION -->	
					<?php if ( get_sub_field('description') ) : ?>
						<p><?php the_sub_field('description'); ?></p>
					<?php endif; ?>
					<!-- BUTTON -->
					<?php
						$link = get_sub_field('button'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self'; 
						if ( get_sub_field('button') ) : 
					?>
						<a class="button is-blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</section>
		<section class="badge-key">
			<div class="badge">
				<svg viewBox="0 0 26 28">
					<use xlink:href="#subta-icon"></use>
				</svg>
			</div>
			= SUBTA partner, SUBTA is the Subscription Trade Association
			<a href="https://subta.com/" target="_blank">
				Join Today!
				<svg viewBox="0 0 26 28">
					<use xlink:href="#arrow-right"></use>
				</svg>
			</a>
		</section>
		<?php if ( $platinum_sponsors->have_posts() ) : ?>
			<section class="sponsor-feed platinum standard">
				<h2>Platinum Sponsors</h2>
				<?php while ( $platinum_sponsors->have_posts() ) : $platinum_sponsors->the_post(); ?>
					<div class="sponsor">
						<?php $logo = get_field('logo'); ?>
						<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
						<?php if ( get_field('subta') == 'yes' ) : ?>
							<div class="badge">
								<svg viewBox="0 0 26 28">
									<use xlink:href="#subta-icon"></use>
								</svg>
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $gold_sponsors->have_posts() ) : ?>
			<section class="sponsor-feed gold is-wide">
				<h2>Gold Sponsors</h2>
				<?php while ( $gold_sponsors->have_posts() ) : $gold_sponsors->the_post(); ?>
					<div class="sponsor">
						<?php $logo = get_field('logo'); ?>
						<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
						<?php if ( get_field('subta') == 'yes' ) : ?>
							<div class="badge">
								<svg viewBox="0 0 26 28">
									<use xlink:href="#subta-icon"></use>
								</svg>
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $silver_sponsors->have_posts() ) : ?>
			<section class="sponsor-feed silver is-wide">
				<h2>Silver Sponsors</h2>
				<?php while ( $silver_sponsors->have_posts() ) : $silver_sponsors->the_post(); ?>
					<div class="sponsor">
						<?php $logo = get_field('logo'); ?>
						<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
						<?php if ( get_field('subta') == 'yes' ) : ?>
							<div class="badge">
								<svg viewBox="0 0 26 28">
									<use xlink:href="#subta-icon"></use>
								</svg>
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $bronze_sponsors->have_posts() ) : ?>
			<section class="sponsor-feed bronze is-wide">
				<h2>Bronze Sponsors</h2>
				<?php while ( $bronze_sponsors->have_posts() ) : $bronze_sponsors->the_post(); ?>
					<div class="sponsor">
						<?php $logo = get_field('logo'); ?>
						<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
						<?php if ( get_field('subta') == 'yes' ) : ?>
							<div class="badge">
								<svg viewBox="0 0 26 28">
									<use xlink:href="#subta-icon"></use>
								</svg>
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $other_sponsors->have_posts() ) : ?>
			<section class="sponsor-feed other is-wide">
				<h2>Additional Sponsors</h2>
				<?php while ( $other_sponsors->have_posts() ) : $other_sponsors->the_post(); ?>
					<div class="sponsor">
						<?php $logo = get_field('logo'); ?>
						<img class="lazyload" data-expand="-100" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
						<?php if ( get_field('subta') == 'yes' ) : ?>
						<div class="badge">
							<svg viewBox="0 0 26 28">
								<use xlink:href="#subta-icon"></use>
							</svg>
						</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; wp_reset_postdata(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>